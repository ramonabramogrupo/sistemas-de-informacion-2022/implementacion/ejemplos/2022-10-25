﻿USE piezas;

-- implementar la division


-- c1
-- listar los proyectos(j) junto con las piezas(p) 
-- que se han suministrado a cada proyecto
SELECT DISTINCT spj.j,spj.p FROM spj;

-- c2
-- listar las piezas distribuidas a algun proyecto
SELECT DISTINCT p FROM spj;

-- c1/c2
-- muestra los proyectos que han utilizado todas las 
-- piezas suministrados alguna vez
SELECT DISTINCT spj.j,spj.p FROM spj
  DIV
SELECT DISTINCT p FROM spj;

-- C3
-- ARREGLAR EL NUMERADOR
-- SOLAMENTE QUIERO QUE EN C1 QUEDEN LAS PIEZAS QUE HAY EN C2
SELECT 
    * 
  FROM 
    (
       SELECT DISTINCT spj.j,spj.p FROM spj
    ) C1
    NATURAL JOIN 
    (
       SELECT DISTINCT p FROM spj
    )C2;

-- TERMINAR LA DIVISION
-- UTILIZAR UNA CONSULTA DE TOTALES
SELECT 
    c3.j
  FROM 
    (
      SELECT 
          * 
        FROM 
          (
             SELECT DISTINCT spj.j,spj.p FROM spj
          ) C1
          NATURAL JOIN 
          (
             SELECT DISTINCT p FROM spj
          )C2  
    ) c3
  GROUP BY c3.j
  HAVING COUNT(*) >=(SELECT COUNT(DISTINCT p) FROM spj);


INSERT INTO spj (s, p, j, cant) VALUES 
  ('s1', 'p1', 'j2', 10),
  ('s1', 'p6', 'j2', 1);






  


