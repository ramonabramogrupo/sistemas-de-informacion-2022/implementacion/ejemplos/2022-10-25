﻿SET NAMES 'utf8';
USE piezas;

-- listar los proyectos
SELECT * FROM j;

-- listar las piezas
SELECT * FROM p;

-- listar los distribuidores
SELECT * FROM s;

-- listar las piezas distribuidas
SELECT * FROM spj;

-- consulta 1
-- nombre de la pieza y nombre del proyecto para el 
-- que se ha suministrado esa pieza

-- realizar la consulta con join
SELECT 
    DISTINCT nomj,nomp     
  FROM p 
      JOIN spj ON p.P = spj.p
      JOIN j ON spj.j = j.j;


-- OPTIMIZAMOS LA CONSULTA


-- C1
SELECT DISTINCT p,j FROM spj;

-- TERMINO LA CONSULTA
SELECT 
    nomj,nomp 
  FROM p 
    JOIN 
      (
        SELECT DISTINCT p,j FROM spj
      ) C1 USING(P)
    JOIN 
      j USING(j);

-- CONSULTA 2
-- NOMBRE DE LAS PIEZAS QUE HAN SIDO SUMINISTRADAS
-- ALGUNA VEZ

-- UTILIZANDO SEMI JOIN (EXISTS)

SELECT 
    nomp 
  FROM p
  WHERE 
    EXISTS (SELECT 1 FROM spj WHERE spj.p=p.P);


-- utilizando un join
 SELECT 
    DISTINCT nomp 
  FROM p 
    JOIN spj ON p.P = spj.p; 

-- utilizando el join optimizado

-- c1
-- piezas suministradas alguna vez
SELECT DISTINCT p FROM spj;

-- termino la consulta
SELECT 
    nomp 
  FROM p JOIN
      (
        SELECT DISTINCT p FROM spj
      ) c1 USING(p);

-- utilizando where in
  
SELECT 
    nomp 
  FROM p     
  WHERE 
    p.P IN (SELECT DISTINCT p FROM spj);

-- CONSULTA 3
-- LISTAR EL NOMBRE DE TODAS LAS PIEZAS 
-- JUNTO CON EL CODIGO DEL PROYECTO Y 
-- EL CODIGO DEL SUMINISTRADOR . 
-- ME DEBE MOSTRAR EL NOMBRE DE TODAS LAS PIEZAS 
-- (INCLUIDAS LAS NO DISTRIBUIDAS)

-- realizad con left join
 SELECT 
    nomp,j,s 
  FROM p LEFT JOIN spj ON p.P = spj.p;


-- CONSULTA 4
-- INDICAME EL NOMBRE DE LAS PIEZAS QUE NO SE 
-- HAN DISTRIBUIDO


-- utilizando directamente el left join con exclusion
SELECT 
    p.nomp 
  FROM p 
    LEFT JOIN spj USING(p)
  WHERE spj.p IS NULL;

-- OPTIMIZACION DE LA CONSULTA

-- c1
-- listar las piezas suministradas
  SELECT DISTINCT p FROM spj;

-- termino la consulta
-- p-c1

-- implemento esa exclusion mediante left join
SELECT 
    nomp 
  FROM p 
    LEFT JOIN
      (
       SELECT DISTINCT p FROM spj 
      ) c1 USING(p)
  WHERE c1.p IS NULL;

-- CONSULTA 4
-- INDICAME EL NOMBRE DE LAS PIEZAS QUE NO SE 
-- HAN DISTRIBUIDO

-- UTILIZANDO ANTI SEMIJOIN

SELECT 
    nomp 
  FROM p 
  WHERE
    NOT EXISTS (SELECT 1 FROM spj WHERE p.p=spj.p);


-- CONSULTA 4
-- INDICAME EL NOMBRE DE LAS PIEZAS QUE NO SE 
-- HAN DISTRIBUIDO

-- UTILIZANDO WHERE CON NOT IN

-- c1
-- listar las piezas suministradas
  SELECT DISTINCT p FROM spj;

-- quiero realizar la exclusion
-- p-c1
SELECT 
    p.nomp 
  FROM p 
  WHERE 
    p.p NOT IN ( );

-- CONSULTA 5
-- INDICAME LAS CIUDADES DONDE HAY PIEZAS Y PROYECTOS

-- C1
-- CIUDADES DONDE HAY PIEZAS
SELECT DISTINCT ciudad FROM p

-- C2
-- CIUDADES DONDE HAY PROYECTOS
SELECT DISTINCT ciudad FROM j

-- REALIZARLO CON INTERSECCION
-- NO FUNCIONA EN MYSQL

SELECT DISTINCT ciudad FROM p
  INTERSECT 
SELECT DISTINCT ciudad FROM j;

-- IMPLEMENTAR LA INTERSECCION UTILIZANDO 
-- NATURAL JOIN

SELECT 
    * 
  FROM 
    (
      SELECT DISTINCT ciudad FROM p
    ) C1
    NATURAL JOIN
    (
      SELECT DISTINCT ciudad FROM j
    ) C2;

-- IMPLEMENTAR CON UN IN
-- IMPLEMENTAR CON UN JOIN
-- IMPLEMENTAR CON UN SEMIJOIN


-- CONSULTA 6
-- INDICAME LAS CIUDADES QUE TIENEN DISTRIBUIDORES 
-- PERO QUE NO HAY PIEZAS

-- C1
-- CIUDADES QUE TIENEN DISTRIDORES
SELECT DISTINCT ciudad FROM s;

-- C2
-- CIUDADES QUE TIENEN PIEZAS
SELECT DISTINCT ciudad FROM P;

-- IMPLEMENTARLO MEDIANTE EXCEPT

SELECT DISTINCT ciudad FROM s
  EXCEPT 
SELECT DISTINCT ciudad FROM p;


-- IMPLEMENTO LA RESTA 
-- CON UN NOT IN
-- C1-C2

 SELECT 
    * 
  FROM 
    (
       SELECT DISTINCT ciudad FROM s
    ) C1
  WHERE 
    c1.ciudad NOT IN (SELECT DISTINCT ciudad FROM p);

  
-- IMPLEMENTO LA RESTA CON 
-- UN LEFT JOIN
SELECT 
    c1.ciudad 
  FROM 
      (
        SELECT DISTINCT ciudad FROM s
      ) C1
    LEFT JOIN
      (
        SELECT DISTINCT ciudad FROM p
      ) C2 USING(ciudad)
  WHERE 
      c2.ciudad IS NULL;

-- IMPLEMENTO CON UN 
-- ANTI SEMIJOIN (NOT EXISTS)
 SELECT 
    * 
  FROM 
    (
      SELECT DISTINCT ciudad FROM s  
    ) C1
  WHERE
    NOT EXISTS (SELECT 1 FROM P WHERE c1.ciudad=p.ciudad);

-- consulta 7
-- piezas que han sido distribuidas a todos los proyectos 
-- a los que se le ha suministrado alguna vez una pieza

-- c1
-- piezas y proyectos a los que se han distribuido
SELECT DISTINCT spj.p,spj.j FROM spj;

-- c2
-- proyectos a los que se han distribuido piezas
SELECT DISTINCT j FROM spj;


-- division
-- c1/c2
SELECT DISTINCT spj.p,spj.j FROM spj
  DIV 
SELECT DISTINCT j FROM spj;

-- IMPLEMENTAR LA DIVISION

-- C3
-- ARREGLO UN POCO EL NUMERADOR
-- PARA QUE SEA UN SUBCONJUNTO DE C2
-- TODOS LOS PROYECTOS DE C1 ESTAN EN C2
SELECT 
    * 
  FROM 
    ( 
       SELECT DISTINCT spj.p,spj.j FROM spj
    ) C1 
  NATURAL JOIN  
    ( 
       SELECT DISTINCT j FROM spj
    ) C2;

-- TERMINAMOS LA DIVISION

SELECT 
    c3.p
  FROM 
    (
      SELECT 
          * 
        FROM 
          ( 
             SELECT DISTINCT spj.p,spj.j FROM spj
          ) C1 
        NATURAL JOIN  
          ( 
             SELECT DISTINCT j FROM spj
          ) C2    
    ) C3
  GROUP BY c3.p
  HAVING 
      COUNT(*) >= (SELECT COUNT(DISTINCT j) FROM spj);
   







               







